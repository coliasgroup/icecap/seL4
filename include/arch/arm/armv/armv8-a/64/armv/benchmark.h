/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#pragma once

#include <config.h>
#ifdef CONFIG_ENABLE_BENCHMARKS

#define CCNT "PMCCNTR_EL0"
#define PMCR "PMCR_EL0"
#define PMCNTENSET "PMCNTENSET_EL0"
#define PMINTENSET "PMINTENSET_EL1"
#define PMOVSR "PMOVSCLR_EL0"
#define CCNT_INDEX 31

#define PMCR_EL0_E 0
#define PMCR_EL0_P 1
#define PMCR_EL0_C 2
#define PMCR_EL0_D 7
#define PMCR_EL0_LC 6
#define PMCR_EL0_LP 7

#define PMCCFILTR_EL0_NSH 27

#define ARMV_PMCR_SET (0 \
    | BIT(PMCR_EL0_E) | BIT(PMCR_EL0_P) | BIT(PMCR_EL0_C) \
    | BIT(PMCR_EL0_LC) | BIT(PMCR_EL0_LP) \
    )

#define ARMV_PMCR_CLEAR (0 \
    | BIT(PMCR_EL0_D) \
    )

static inline void armv_enableOverflowIRQ(void)
{
    uint32_t val;
    MRS(PMINTENSET, val);
    val |= BIT(CCNT_INDEX);
    MSR(PMINTENSET, val);
}

static inline void armv_handleOverflowIRQ(void)
{
    assert(0);
    uint32_t val = BIT(CCNT_INDEX);
    MSR(PMOVSR, val);
}

#endif /* CONFIG_ENABLE_BENCHMARKS */

